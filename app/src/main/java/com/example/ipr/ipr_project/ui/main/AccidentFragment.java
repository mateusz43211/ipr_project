package com.example.ipr.ipr_project.ui.main;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.ipr.ipr_project.Constants;
import com.example.ipr.ipr_project.R;
import com.example.ipr.ipr_project.Serialization.Vehicle;
import com.example.ipr.ipr_project.SharedViewModel;

public class AccidentFragment extends Fragment {
    public interface OnAccidentFragmentInteractionListener {
        void onAcceptButtonClicked();
    }

    private OnAccidentFragmentInteractionListener mListener;
    private ImageButton acceptButton;
    private TextView wybranoPojazdTextView;
    private SharedViewModel mSharedViewModel;

    public static AccidentFragment newInstance() {
        return new AccidentFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_accident, container, false);
        acceptButton = rootView.findViewById(R.id.zatwierdzButton);
        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onAcceptButtonClicked();
            }
        });

        wybranoPojazdTextView = rootView.findViewById(R.id.wybranoPojazdTextView);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Fragment vehicleListFragment = new VehicleListFragment();
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.replace(R.id.accident_vehicles_container, vehicleListFragment, Constants.VEHICLE_LIST_FRAGMENT_TAG);
        ft.commitNow();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mSharedViewModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);

        Observer<Vehicle> observer = new Observer<Vehicle>() {
            @Override
            public void onChanged(@Nullable Vehicle vehicle) {
                String newText = "Wybrano " + vehicle.getName();
                wybranoPojazdTextView.setText(newText);
            }
        };

        mSharedViewModel.getAccidentChosenVehicle().observe(this, observer);

        LiveData<Vehicle> defaultVehicle = mSharedViewModel.getChosenVehicle();
        mSharedViewModel.setAccidentChosenVehicle(defaultVehicle.getValue());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAccidentFragmentInteractionListener) {
            mListener = (OnAccidentFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                                               + " must implement OnAccidentFragmentInteractionListener");
        }
    }
}
