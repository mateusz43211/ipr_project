package com.example.ipr.ipr_project.Logic;

import com.example.ipr.ipr_project.Serialization.ConfigurationSheet;
import com.example.ipr.ipr_project.Serialization.Vehicle;
import com.example.ipr.ipr_project.Serialization.VehicleStatus;

import java.util.ArrayList;

public class ParameterList extends ArrayList<String> {
    public ParameterList() {

    }

    public void setParameters(Vehicle vehicle) {
        if (!this.isEmpty()){
            clear();
        }

        String tmp;
        String separator = ": ";
        ConfigurationSheet cs = vehicle.ConfigurationSheet;
        VehicleStatus s = vehicle.LastStatus;

        tmp = cs.InputConfiguration.DI1 + separator + String.valueOf(s.IN1);
        add(tmp);

        tmp = cs.InputConfiguration.DI2 + separator + String.valueOf(s.IN2);
        add(tmp);

        tmp = cs.InputConfiguration.DI3 + separator + String.valueOf(s.IN3);
        add(tmp);

        tmp = cs.InputConfiguration.DI4 + separator + String.valueOf(s.IN4);
        add(tmp);

        tmp = cs.InputConfiguration.DI5 + separator + String.valueOf(s.IN5);
        add(tmp);

        tmp = cs.InputConfiguration.DI6 + separator + getBooleanString(s.IN6);
        add(tmp);

        tmp = cs.InputConfiguration.DI7 + separator + getBooleanString(s.IN7);
        add(tmp);

        tmp = cs.InputConfiguration.DI8 + separator + getBooleanString(s.IN8);
        add(tmp);

        tmp = cs.InputConfiguration.DI9 + separator + getBooleanString(s.IN9);
        add(tmp);
    }

    private String getBooleanString(boolean value){
        if (value){
            return "TAK";
        }
        return "NIE";
    }
}


