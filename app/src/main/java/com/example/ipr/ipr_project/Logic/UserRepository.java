package com.example.ipr.ipr_project.Logic;

public class UserRepository implements IUserRepository {
    private ApiClient apiClient;


    public UserRepository (){
        apiClient = new ApiClient();
    }

    @Override
    public int getUserID(String login, String password) {
       String readString = apiClient.getUserID(login, password);

       if (readString != null) {
            if (readString.length() != 0){
                return Integer.valueOf(readString);
            }
            else {
                return Constants.BAD_LOGIN_PASSWORD_CODE;
            }
       }
       return Constants.CONNECTION_NOT_WORKING;
    }
}
