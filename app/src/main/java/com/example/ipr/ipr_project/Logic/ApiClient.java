package com.example.ipr.ipr_project.Logic;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.common.util.IOUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ApiClient {
    private final String TAG = "ApiClient";
    private String vehiclePath = "https://szbdsrv.azurewebsites.net/api/vehicles/";
    private String userPath = "https://szbdsrv.azurewebsites.net/api/users/";
    private String datePattern = "yyyy-MM-dd";
    private SimpleDateFormat dateFormat = new SimpleDateFormat(datePattern);
    private int RESULT_OK_CODE = 200;
    private int RESULT_NOT_FOUND = 404;
    private int TIMEOUT_IN_MILIS = 10000;

    public ApiClient() {

    }

    public boolean sendDamage( int vehicleID) {
        return true;
    }

    public byte[] getOperatorsVehicles(final String operatorID) {
        GetOperatorsVehiclesTask task = new GetOperatorsVehiclesTask(operatorID);
        try {
            return task.execute().get(TIMEOUT_IN_MILIS, TimeUnit.MILLISECONDS);
        } catch (ExecutionException | InterruptedException | TimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }

    public byte[] getVehicleStatusesFromTo(final String vehicleID, final Date from,
                                           final Date to) {
        GetVehicleStatusesTask task = new GetVehicleStatusesTask(vehicleID, from, to);
        try {
            return task.execute().get(TIMEOUT_IN_MILIS, TimeUnit.MILLISECONDS);
        } catch (ExecutionException | InterruptedException | TimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }

    public byte[] getLastVehicleStatus(int vehicleID) {
        GetLastVehicleStatus task = new GetLastVehicleStatus(vehicleID);
        try {
            return task.execute().get(TIMEOUT_IN_MILIS, TimeUnit.MILLISECONDS);
        } catch (ExecutionException | InterruptedException | TimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getUserID(String login, String password) {
        GetUserIDTask task = new GetUserIDTask(login,password);
        try {
            return task.execute().get(TIMEOUT_IN_MILIS, TimeUnit.MILLISECONDS);
        } catch (ExecutionException | InterruptedException | TimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }

    private class GetVehicleStatusesTask extends AsyncTask<Void, Void, byte[]> {
        private String urlPath;

        public GetVehicleStatusesTask(String vehicleID, Date from, Date to) {
            super();
            String dateFromString = dateFormat.format(from);
            String dateToString = dateFormat.format(to);

            urlPath = vehiclePath + "statuses?vehicleId=" + vehicleID;
            urlPath += "&from=" + dateFromString + "&to" + dateToString;
        }

        @Override
        protected byte[] doInBackground(Void... voids) {
            try {
                URL url = new URL(urlPath);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                int response = connection.getResponseCode();
                if (response == RESULT_OK_CODE) {
                    InputStream inputStream = connection.getInputStream();
                    byte[] byteArray = IOUtils.toByteArray(inputStream);
                    connection.disconnect();
                    return byteArray;
                } else {
                    Log.e(TAG, "GetVehicleStatusesTask bad result code");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private class GetOperatorsVehiclesTask extends AsyncTask<Void, Void, byte[]> {
        private String urlPath;

        public GetOperatorsVehiclesTask(String operatorID) {
            super();
            urlPath = vehiclePath + "operator/" + operatorID;
        }

        @Override
        protected byte[] doInBackground(Void... voids) {
            try {
                URL url = new URL(urlPath);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                int response = connection.getResponseCode();
                if (response == RESULT_OK_CODE) {
                    InputStream inputStream = connection.getInputStream();
                    byte[] byteArray = IOUtils.toByteArray(inputStream);
                    connection.disconnect();
                    return byteArray;
                } else {
                    Log.e(TAG, "GetVehicleStatusesTask bad result code");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class GetLastVehicleStatus extends AsyncTask<Void, Void, byte[]> {
        private String urlPath;

        GetLastVehicleStatus(int vehicleID) {
            super();
            urlPath = vehiclePath + "lastStatus/" + String.valueOf(vehicleID);
        }

        @Override
        protected byte[] doInBackground(Void... voids) {
            try {
                URL url = new URL(urlPath);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                int response = connection.getResponseCode();
                if (response == RESULT_OK_CODE) {
                    InputStream inputStream = connection.getInputStream();
                    byte[] byteArray = IOUtils.toByteArray(inputStream);
                    connection.disconnect();
                    return byteArray;
                } else {
                    Log.e(TAG, "GetLastVehicleStatus bad result code");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    //api/users/login?login=operator3&password=haslo3
    public class GetUserIDTask extends AsyncTask<Void, Void, String> {
        private final String urlPath;

        public GetUserIDTask(String login, String password) {
            super();
            urlPath = userPath + "/login?login=" + login + "&password=" + password;
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {
                URL url = new URL(urlPath);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                int response = connection.getResponseCode();
                if (response == RESULT_OK_CODE) {
                    InputStream inputStream = connection.getInputStream();
                    String result = convertStreamToString(inputStream);
                    String number = result.substring(0, result.length() - 1);
                    return number;
                } else if (response == RESULT_NOT_FOUND) {
                    // password or login are incorrect
                    return "";
                } else {
                    Log.e(TAG, "GetLastVehicleStatus bad result code");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }


    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

}
