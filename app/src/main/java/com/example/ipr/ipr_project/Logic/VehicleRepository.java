package com.example.ipr.ipr_project.Logic;

import android.os.AsyncTask;

import com.example.ipr.ipr_project.Serialization.Vehicle;
import com.example.ipr.ipr_project.Serialization.VehicleStatus;
import com.google.gson.Gson;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class VehicleRepository implements IVehicleRepository {

    private ArrayList<Vehicle> vehicleArrayList;
    private ApiClient apiClient = new ApiClient();

    @Override
    public boolean sendDamage(int vehicleID) {
        return apiClient.sendDamage(vehicleID);
    }

    @Override
    public void loadAllOperatorsVehicles(int operatorID) {
        try {
            byte[] byteArray = apiClient.getOperatorsVehicles(String.valueOf(operatorID));
            InputStream inputStream = new ByteArrayInputStream(byteArray);
            InputStreamReader responseBodyReader = new InputStreamReader(inputStream, "UTF-8");

            Gson gson = new Gson();
            Vehicle vehicles[] = gson.fromJson(responseBodyReader, Vehicle[].class);
            List<Vehicle> vehicleList = Arrays.asList(vehicles);

            sortVehicleList(vehicleList);
            saveVehiclesInRepository(vehicleList);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveVehiclesInRepository(List<Vehicle> vehicleList) {
        vehicleArrayList = new ArrayList<>(vehicleList.size());
        vehicleArrayList.addAll(vehicleList);
    }

    private void sortVehicleList(List<Vehicle> vehicleList) {
        Collections.sort(vehicleList, new Comparator<Vehicle>() {
            @Override
            public int compare(Vehicle o1, Vehicle o2) {
                return o1.Name.compareTo(o2.Name);
            }
        });
    }

    @Override
    public List<VehicleStatus> getStatusesFromTo(Date from, Date to, int vehicleID) {

        byte[] byteArray = apiClient.getVehicleStatusesFromTo(String.valueOf(vehicleID), from, to);
        InputStream inputStream = new ByteArrayInputStream(byteArray);
        InputStreamReader responseBodyReader = null;
        try {
            responseBodyReader = new InputStreamReader(inputStream, "UTF-8");
            Gson gson = new Gson();
            VehicleStatus vehicleStatuses[] = gson.fromJson(responseBodyReader, VehicleStatus[].class);

            List<VehicleStatus> vehicleStatusList = Arrays.asList(vehicleStatuses);

            Collections.sort(vehicleStatusList, new Comparator<VehicleStatus>() {
                @Override
                public int compare(VehicleStatus o1, VehicleStatus o2) {
                    return o1.TimeStamp.compareTo(o2.TimeStamp);
                }
            });

            return vehicleStatusList;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public ArrayList<Vehicle> getAllVehicles() {
        return vehicleArrayList;
    }

    @Override
    public VehicleStatus getLastVehicleStatus(int vehicleID) {
        byte[] byteArray = apiClient.getLastVehicleStatus(vehicleID);
        InputStream inputStream = new ByteArrayInputStream(byteArray);
        InputStreamReader responseBodyReader = null;
        try {
            responseBodyReader = new InputStreamReader(inputStream, "UTF-8");
            Gson gson = new Gson();
            VehicleStatus lastStatus = gson.fromJson(responseBodyReader, VehicleStatus.class);
            return lastStatus;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
