package com.example.ipr.ipr_project.ui.main;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ipr.ipr_project.R;
import com.example.ipr.ipr_project.Serialization.Vehicle;
import com.example.ipr.ipr_project.Serialization.VehicleStatus;
import com.example.ipr.ipr_project.SharedViewModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;

import java.util.Date;
import java.util.List;

public class HistoryMapFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap map;
    MapView mapView;
    private View rootView;
    private Marker lastMarker;
    private SharedViewModel mSharedViewModel;

    public HistoryMapFragment() {
        // Required empty public constructor
    }

    public static HistoryMapFragment newInstance() {
        HistoryMapFragment fragment = new HistoryMapFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        try {
            rootView = inflater.inflate(R.layout.fragment_my_map, container, false);
            MapsInitializer.initialize(getActivity());
            mapView = (MapView) rootView.findViewById(R.id.mapView);
            mapView.onCreate(savedInstanceState);
            mapView.getMapAsync(this);
        }
        catch (InflateException e){
            Log.e("TAG", "Inflate exception");
        }
        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        UiSettings settings = map.getUiSettings();
        settings.setCompassEnabled(true);
        settings.setZoomControlsEnabled(true);
        settings.setAllGesturesEnabled(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSharedViewModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);

        final Observer<Vehicle> vehicleObserver = new Observer<Vehicle>() {
            @Override
            public void onChanged(@Nullable Vehicle vehicle) {
                loadDataToMap();
            }
        };
        mSharedViewModel.getLocalizationChosenVehicle().observe(this, vehicleObserver);
    }

    private void loadDataToMap() {
        Date begin = getBeginDate();
        Date end = getEndDate();
        Vehicle chosenVehicle = getChosenVehicle();

        List<VehicleStatus> vehicleStatuses = getStatusesFromTo(begin, end, chosenVehicle);
        drawPathOnMap(vehicleStatuses);
    }

    private List<VehicleStatus> getStatusesFromTo(Date begin, Date end, Vehicle chosenVehicle) {
        return mSharedViewModel.vehicleRepository.
                getStatusesFromTo(begin,end,Integer.valueOf(chosenVehicle.Id));
    }

    private Vehicle getChosenVehicle() {
        return mSharedViewModel.getLocalizationChosenVehicle().getValue();
    }

    private Date getEndDate() {
        return mSharedViewModel.getDateTo().getValue();
    }

    private Date getBeginDate() {
        return mSharedViewModel.getDateFrom().getValue();
    }

    private void drawPathOnMap(List<VehicleStatus> vehicleStatuses) {
        if (map != null) {
            map.clear();

            if (lastMarker != null){
                lastMarker.remove();
            }

            LatLng coordinates [] = getCoordinatesFrom(vehicleStatuses);

            Polyline polyline = map.addPolyline(new PolylineOptions().clickable(false).add(
                    getCoordinatesFrom(vehicleStatuses)
            ));

            polyline.setTag("B");
            stylePolyline(polyline);

            int lastStatusIndex = vehicleStatuses.size() - 1;
            lastMarker = map.addMarker(new MarkerOptions().position(coordinates[lastStatusIndex]).title("Ostatnia lokalizacja"));
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates[lastStatusIndex], 13));
            mapView.onResume();
        }
    }

    private LatLng[] getCoordinatesFrom(List<VehicleStatus> vehicleStatuses) {
        LatLng result [] = new LatLng[vehicleStatuses.size()];
        VehicleStatus tmp;
        for (int i=0 ; i<vehicleStatuses.size(); i++){
            tmp = vehicleStatuses.get(i);
            result [i] = new LatLng(tmp.Latitude, tmp.Longitude);
        }
        return result;
    }

    private static final int POLYLINE_STROKE_WIDTH_PX = 6;
    private static final int LINE_COLOR_ARGB = 0xff9f2fff;

    private void stylePolyline(Polyline polyline) {
        String type = "";
        // Get the data object stored with the polyline.
        if (polyline.getTag() != null) {
            type = polyline.getTag().toString();
        }

        switch (type) {
            // If no type is given, allow the API to use the default.
            case "A":
                // Use a custom bitmap as the cap at the start of the line.
//                polyline.setStartCap(
//                        new CustomCap(
//                                BitmapDescriptorFactory.fromResource(R.drawable.ic_arrow), 10));
                break;
            case "B":
                // Use a round cap at the start of the line.
                polyline.setStartCap(new RoundCap());
                break;
        }

        polyline.setEndCap(new RoundCap());
        polyline.setWidth(POLYLINE_STROKE_WIDTH_PX);
        polyline.setColor(LINE_COLOR_ARGB);
        polyline.setJointType(JointType.ROUND);
    }
}
