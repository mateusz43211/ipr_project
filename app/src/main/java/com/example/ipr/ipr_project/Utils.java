package com.example.ipr.ipr_project;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Utils {
    public static void showToastWithMessage(Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);

        TextView v = toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();
    }

    public static void hideButton(ImageButton button) {
        button.setEnabled(false);
        button.setVisibility(View.INVISIBLE);
    }


    public static void showButton(ImageButton button) {
        button.setEnabled(true);
        button.setVisibility(View.VISIBLE);
    }

    public static void hideButton(Button button) {
        button.setEnabled(false);
        button.setVisibility(View.INVISIBLE);
    }


    public static void showButton(Button button) {
        button.setEnabled(true);
        button.setVisibility(View.VISIBLE);
    }

    public static void hideImageView(ImageView imageView) {
        imageView.setVisibility(View.INVISIBLE);
        imageView.setEnabled(false);
    }
    public static void showImageView(ImageView imageView) {
        imageView.setVisibility(View.VISIBLE);
        imageView.setEnabled(true);
    }

    public static void hideTextView(TextView textView) {
        textView.setVisibility(View.INVISIBLE);
        textView.setEnabled(false);
    }
    public static void showTextView(TextView textView) {
        textView.setVisibility(View.VISIBLE);
        textView.setEnabled(true);
    }





}
