package com.example.ipr.ipr_project.ui.main;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ipr.ipr_project.R;
import com.example.ipr.ipr_project.Serialization.Vehicle;
import com.example.ipr.ipr_project.Serialization.VehicleStatus;
import com.example.ipr.ipr_project.SharedViewModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class SimpleMapFragment extends Fragment implements OnMapReadyCallback {
    private GoogleMap map;
    private MapView mapView;
    private View rootView;
    private Marker lastMarker;
    private SharedViewModel sharedViewModel;

    public static SimpleMapFragment newInstance() {
        SimpleMapFragment fragment = new SimpleMapFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            rootView = inflater.inflate(R.layout.fragment_my_map, container, false);
            MapsInitializer.initialize(getActivity());
            mapView = (MapView) rootView.findViewById(R.id.mapView);
            mapView.onCreate(savedInstanceState);
            mapView.getMapAsync(this);
        }
        catch (InflateException e){
            Log.e("TAG", "Inflate exception");
        }

        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.getUiSettings().setZoomControlsEnabled(true);

        Vehicle choosenVehicle = sharedViewModel.getChosenVehicle().getValue();
        setMapPosition(choosenVehicle.LastStatus);
    }

    public void setMapPosition(VehicleStatus status) {
        if (map != null) {
            map.clear();
            LatLng currentPlace = new LatLng(status.Latitude, status.Longitude);

            if (lastMarker != null){
                lastMarker.remove();
            }

            lastMarker = map.addMarker(new MarkerOptions().position(currentPlace).title("Obecna lokalizacja"));
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(currentPlace,13));
            mapView.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        sharedViewModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
        final Observer<Vehicle> vehicleObserver = new Observer<Vehicle>() {
            @Override
            public void onChanged(@Nullable Vehicle vehicle) {
                setMapPosition(vehicle.LastStatus);
            }
        };
        sharedViewModel.getVehicleLocalizationChosenVehicle().observe(this, vehicleObserver);
    }
}
