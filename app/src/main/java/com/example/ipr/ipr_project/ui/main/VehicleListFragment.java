package com.example.ipr.ipr_project.ui.main;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ipr.ipr_project.R;
import com.example.ipr.ipr_project.Serialization.Vehicle;
import com.example.ipr.ipr_project.SharedViewModel;

import java.util.ArrayList;

public class VehicleListFragment extends ListFragment {
    private ArrayList<Vehicle> vehicles;
    private ArrayList<Vehicle> currentListVehicle;
    private SharedViewModel mSharedViewModel;
    private ArrayAdapter<Vehicle> adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadSharedViewModel();
        vehicles = mSharedViewModel.vehicleRepository.getAllVehicles();
        displayListOfVehicles();
    }

    private void loadSharedViewModel() {
        mSharedViewModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
    }

    private void displayListOfVehicles() {
        currentListVehicle = new ArrayList<>(vehicles);
        adapter = new ArrayAdapter<Vehicle>(getActivity(), R.layout.list_item, currentListVehicle);
        setListAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        adapter.notifyDataSetChanged();
        setSelection(position);

        Vehicle vehicle = adapter.getItem(position);

        Fragment fragment = getParentFragment();
        if (fragment instanceof LocalizationFragment) {
            mSharedViewModel.setLocalizationChosenVehicle(vehicle);
            Toast.makeText(getActivity(), vehicle.getName(), Toast.LENGTH_SHORT).show();
        }
        else if (fragment instanceof ChooseVehicleFragment) {
            mSharedViewModel.setChosenVehicle(vehicle);
        }
        else if (fragment instanceof VehicleLocalizationFragment) {
            mSharedViewModel.setVehicleLocalizationChosenVehicle(vehicle);
        }
        else if (fragment instanceof AccidentFragment) {
            mSharedViewModel.setAccidentChosenVehicle(vehicle);
        }
    }

    public void filterList(ArrayList<Vehicle> filteredVehicles) {
        currentListVehicle.clear();
        currentListVehicle.addAll(filteredVehicles);
        adapter.notifyDataSetChanged();
    }


}
