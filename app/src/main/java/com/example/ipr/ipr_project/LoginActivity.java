package com.example.ipr.ipr_project;

import android.app.Activity;
import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.example.ipr.ipr_project.Logic.Constants;
import com.example.ipr.ipr_project.Logic.IUserRepository;
import com.example.ipr.ipr_project.Logic.UserRepository;
import com.example.ipr.ipr_project.Serialization.Vehicle;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class LoginActivity extends AppCompatActivity {

    ImageButton loginButton, exitButton;
    ImageButton yesButton, noButton;
    EditText loginEditText, passwordEditText;
    FrameLayout exitFrameLayout, loadingFrameLayout;
    CheckBox rememberCheckBox;
    ProgressBar progressBar;
    IUserRepository userRepository = new UserRepository();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setupUI();
    }

    private void setupUI() {
        exitFrameLayout = findViewById(R.id.exitFrameLayout);
        loadingFrameLayout = findViewById(R.id.loadingFrameLayout);

        yesButton = findViewById(R.id.yesButton);
        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        noButton = findViewById(R.id.noButton);
        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitButton.setVisibility(View.VISIBLE);
                exitFrameLayout.setVisibility(View.INVISIBLE);
                exitFrameLayout.setEnabled(false);
            }
        });

        exitButton = findViewById(R.id.exitButton);
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // hide keyboard if open
                hideKeyboardIfOpen(LoginActivity.this);

                exitFrameLayout.setEnabled(true);
                exitFrameLayout.setVisibility(View.VISIBLE);
                exitButton.setVisibility(View.INVISIBLE);
            }
        });



        loginButton = findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                showLoadingView(true);
                String login = loginEditText.getText().toString();
                String password = passwordEditText.getText().toString();

                ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
                progressDialog.setMessage("Loading...");
                progressDialog.show();
                Integer userID = userRepository.getUserID(login,password);

                if (userID == Constants.BAD_LOGIN_PASSWORD_CODE){
                    progressDialog.dismiss();
                    showLoadingView(false);
                    String message = "Nieprawidłowy login lub hasło";
                    Utils.showToastWithMessage(getApplication(), message);
                }
                else if (userID == Constants.CONNECTION_NOT_WORKING){
                    progressDialog.dismiss();
                    showLoadingView(false);
                    String message = "Nie uzyskano połączenia z serwerem";
                    Utils.showToastWithMessage(getApplication(), message);
                }
                else{
                    // TODO login correct
                    if (rememberCheckBox.isChecked()) {
                        // TODO remember user login
                    }
                    progressDialog.dismiss();
                    createMainActivity(userID, login);
                }
            }
        });

        loginEditText = findViewById(R.id.loginEditText);
        loginEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        passwordEditText = findViewById(R.id.passwordEditText);
        passwordEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        rememberCheckBox = findViewById(R.id.rememberCheckBox);

        int color = getResources().getColor(R.color.textColor);
        progressBar = findViewById(R.id.progressBar);
    }

    private void createMainActivity(int userID, String userName) {
        hideKeyboardIfOpen(this);
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.putExtra("operatorID", userID);
        intent.putExtra("operatorName", userName);
        startActivity(intent);
    }

    private void hideKeyboardIfOpen(Activity activity) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (imm.isAcceptingText()) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void showLoadingView(boolean show){
        if (show == true) {
            loadingFrameLayout.setEnabled(true);
            loadingFrameLayout.setVisibility(View.VISIBLE);
        }
        else {
            loadingFrameLayout.setEnabled(false);
            loadingFrameLayout.setVisibility(View.INVISIBLE);
        }
    }
}
