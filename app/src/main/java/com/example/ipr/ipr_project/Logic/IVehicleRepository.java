package com.example.ipr.ipr_project.Logic;

import com.example.ipr.ipr_project.Serialization.Vehicle;
import com.example.ipr.ipr_project.Serialization.VehicleStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public interface IVehicleRepository {
    void loadAllOperatorsVehicles(int operatorID);
    List<VehicleStatus> getStatusesFromTo(Date from, Date to, int vehicleID);
    ArrayList<Vehicle> getAllVehicles();
    VehicleStatus getLastVehicleStatus(int vehicleID);
    boolean sendDamage(int vehicleID);
}
