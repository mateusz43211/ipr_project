package com.example.ipr.ipr_project.Serialization;

import java.util.Date;

public class VehicleStatus {
    public int IN1, IN2, IN3, IN4, IN5;
    public boolean IN6, IN7, IN8, IN9;
    public double Latitude, Longitude;
    public Date TimeStamp;
}
