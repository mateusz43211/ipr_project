package com.example.ipr.ipr_project;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.ipr.ipr_project.Logic.IVehicleRepository;
import com.example.ipr.ipr_project.Logic.VehicleRepository;
import com.example.ipr.ipr_project.Serialization.Vehicle;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class SharedViewModel extends ViewModel {
    private final MutableLiveData<Vehicle> chosenVehicle = new MutableLiveData<>();
    private final MutableLiveData<Vehicle> accidentChosenVehicle = new MutableLiveData<>();
    private final MutableLiveData<Vehicle> localizationChosenVehicle = new MutableLiveData<>();
    private final MutableLiveData<Vehicle> vehicleLocalizationChosenVehicle = new MutableLiveData<>();

    private final MutableLiveData<Date> dateFrom = new MutableLiveData<>();
    private final MutableLiveData<Date> dateTo = new MutableLiveData<>();

    public IVehicleRepository vehicleRepository = new VehicleRepository();

    public void setChosenVehicle(Vehicle Vehicle) {
        chosenVehicle.setValue(Vehicle);
    }

    public LiveData<Vehicle> getChosenVehicle() {
        return chosenVehicle;
    }

    public void setAccidentChosenVehicle(Vehicle Vehicle) {
        accidentChosenVehicle.setValue(Vehicle);
    }

    public LiveData<Vehicle> getAccidentChosenVehicle() {
        return accidentChosenVehicle;
    }

    public LiveData<Date> getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date date) {
        dateFrom.setValue(date);
    }

    public LiveData<Date> getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date date) {
        dateTo.setValue(date);
    }

    public void setDefaultDates() {
        if (dateFrom.getValue() == null && dateTo.getValue() == null) {
            dateFrom.setValue(new Date());
            dateTo.setValue(new Date());
        }

        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("Europe/Warsaw"));
        setDateTo(cal.getTime());
        cal.add(Calendar.HOUR, -1);
        setDateFrom(cal.getTime());
    }

    public void setLocalizationChosenVehicle(Vehicle Vehicle) {
        localizationChosenVehicle.setValue(Vehicle);
    }

    public LiveData<Vehicle> getLocalizationChosenVehicle() {
        return localizationChosenVehicle;
    }

    public void setVehicleLocalizationChosenVehicle(Vehicle Vehicle) {
        vehicleLocalizationChosenVehicle.setValue(Vehicle);
    }

    public LiveData<Vehicle> getVehicleLocalizationChosenVehicle() {
        return vehicleLocalizationChosenVehicle;
    }

    public void loadOperatorVehicles(int operatorID){
        vehicleRepository.loadAllOperatorsVehicles(operatorID);
    }

    public boolean reportDamageOfVehicle(){
        return vehicleRepository.sendDamage(Integer.valueOf(accidentChosenVehicle.getValue().Id));
    }
}
