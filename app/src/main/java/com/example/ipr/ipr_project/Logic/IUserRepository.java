package com.example.ipr.ipr_project.Logic;

public interface IUserRepository {
    int getUserID(String login, String password);
}
