package com.example.ipr.ipr_project;

public class Constants {
    public static final String LOCALIZATION_FRAGMENT_TAG = "Localization";
    public static final String VEHICLES_FRAGMENT_TAG = "Vehicles";
    public static final String CHOOSE_VEHICLE_FRAGMENT_TAG = "ChooseVehicle";
    public static final String MAIN_FRAGMENT_TAG = "Main";
    public static final String ACCIDENT_FRAGMENT_TAG = "Accident";
    public static final String VEHICLE_LOCALIZATION_FRAGMENT_TAG = "VehicleLocalization";
    public static final String VLF_VEHICLE_LIST_TAG = "vlf_vehicle_list";
    public static final String VLF_PARAMETER_LIST_TAG = "vlf_parameter_list";
    public static final String VLF_MAP_TAG = "vlf_map";
    public static final String VEHICLE_LIST_FRAGMENT_TAG = "VehicleListFragment";

}
