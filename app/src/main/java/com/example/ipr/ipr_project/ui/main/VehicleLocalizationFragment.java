package com.example.ipr.ipr_project.ui.main;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ipr.ipr_project.Constants;
import com.example.ipr.ipr_project.R;
import com.example.ipr.ipr_project.Serialization.Vehicle;
import com.example.ipr.ipr_project.Serialization.VehicleStatus;
import com.example.ipr.ipr_project.SharedViewModel;

import java.util.Timer;
import java.util.TimerTask;

public class VehicleLocalizationFragment extends Fragment {
    SimpleMapFragment mapFragment;
    VehicleListFragment vehicleListFragment;
    ParameterListFragment parameterListFragment;
    SharedViewModel mSharedViewModel;
    Timer timer;
    final long WAIT_TIME = 30000;

    public VehicleLocalizationFragment() {
        // Required empty public constructor
    }

    public static VehicleLocalizationFragment newInstance() {
        VehicleLocalizationFragment fragment = new VehicleLocalizationFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            mapFragment = SimpleMapFragment.newInstance();
            vehicleListFragment = new VehicleListFragment();
            parameterListFragment = new ParameterListFragment();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_vehicle_localization, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.add(R.id.vehicle_container, vehicleListFragment, Constants.VEHICLE_LIST_FRAGMENT_TAG);
        ft.add(R.id.parameters_container, parameterListFragment, Constants.VLF_PARAMETER_LIST_TAG);
        ft.add(R.id.map_container, mapFragment, Constants.VLF_MAP_TAG);
        ft.commitNow();
    }



    public void showParametersForVehicle(Vehicle vehicle) {

    }

    private Fragment getNewInstanceFragmentForTag(String tag) {
        switch (tag){
            case Constants.VEHICLE_LIST_FRAGMENT_TAG: {
                return new VehicleListFragment();
            }
            case Constants.VLF_PARAMETER_LIST_TAG: {
                return new ParameterListFragment();
            }
            case Constants.VLF_MAP_TAG: {
                return SimpleMapFragment.newInstance();
            }
            default:
                return null;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadSharedViewModel();

        loadDefaultMap();
        timer = new Timer();
        configureTimer(timer);
    }

    private void configureTimer(Timer timer) {
        UpdateTimerTask task = new UpdateTimerTask();
        timer.scheduleAtFixedRate(task,500, WAIT_TIME);
    }

    private class UpdateTimerTask extends TimerTask{
        @Override
        public void run() {
            updateVehicleStatus();
        }
    }

    private void loadDefaultMap() {
        Vehicle chosenVehicle = mSharedViewModel.getChosenVehicle().getValue();
        mSharedViewModel.setVehicleLocalizationChosenVehicle(chosenVehicle);
    }

    private void loadSharedViewModel() {
        mSharedViewModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
    }

    private void updateVehicleStatus() {
        Vehicle chosenVehicle = mSharedViewModel.getVehicleLocalizationChosenVehicle().getValue();
        VehicleStatus lastVehicleStatus =
                mSharedViewModel.vehicleRepository.getLastVehicleStatus(Integer.valueOf(chosenVehicle.Id));

        chosenVehicle.LastStatus = lastVehicleStatus;
        mSharedViewModel.setVehicleLocalizationChosenVehicle(chosenVehicle);
    }


}
