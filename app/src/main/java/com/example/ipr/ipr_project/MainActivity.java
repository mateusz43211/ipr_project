package com.example.ipr.ipr_project;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.ipr.ipr_project.Serialization.Vehicle;
import com.example.ipr.ipr_project.ui.main.AccidentFragment;
import com.example.ipr.ipr_project.ui.main.ChooseVehicleFragment;
import com.example.ipr.ipr_project.ui.main.LocalizationFragment;
import com.example.ipr.ipr_project.ui.main.MainFragment;
import com.example.ipr.ipr_project.ui.main.VehicleFragment;
import com.example.ipr.ipr_project.ui.main.VehicleListFragment;
import com.example.ipr.ipr_project.ui.main.VehicleLocalizationFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity implements
        ChooseVehicleFragment.OnChooseVehicleFragmentInteractionListener,
        VehicleFragment.OnVehicleFragmentInteractionListener,
        AccidentFragment.OnAccidentFragmentInteractionListener,
        DatePickerDialog.OnDateSetListener,
        TimePickerDialog.OnTimeSetListener {


    DateTimeHolder currentDateTimeHolder = new DateTimeHolder();
    LiveData<Date> currentDate;
    ImageButton loadHistoryOfVehicleButton, backButton, changeVehicleButton;
    ImageButton searchVehicleButton, clearTextEditButton, homeButton;
    Button fromDateButton, toDateButton;
    TextView clockTextView, searchVehicleTextEdit, chosenVehicleTextView;

    ImageView currentWinndowView;
    SharedViewModel mSharedViewModel;
    SimpleDateFormat simpleDateFormat;
    SimpleDateFormat clockDateFromat;
    String operatorName = "Operator 1";
    ConstraintLayout searchToolbar;
    int operatorID = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.widok_glowny, MainFragment.newInstance(), Constants.MAIN_FRAGMENT_TAG)
                    .commitNow();
        }
        // load operator from login activity
        operatorID = getIntent().getIntExtra("operatorID", 3);
        operatorName = getIntent().getStringExtra("operatorName");

        mSharedViewModel = ViewModelProviders.of(this).get(SharedViewModel.class);

        setupUI();

        // clock
        Thread myThread;
        Runnable runnable = new ClockRunner();
        myThread= new Thread(runnable);
        myThread.start();

        mSharedViewModel.loadOperatorVehicles(operatorID);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    private void setupUI() {
        simpleDateFormat = new SimpleDateFormat("yyyy:MM:dd HH:mm");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Poland"));

        clockDateFromat = new SimpleDateFormat("EEE yyyy:MM:dd HH:mm:ss");
        clockDateFromat.setTimeZone(TimeZone.getTimeZone("Poland"));

        ImageButton logoutButton = findViewById(R.id.wylogujButton);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        loadHistoryOfVehicleButton = findViewById(R.id.wczytajButton);
        loadHistoryOfVehicleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    LiveData<Vehicle> liveData = mSharedViewModel.getLocalizationChosenVehicle();
                    Vehicle vehicle = liveData.getValue();
                    mSharedViewModel.setLocalizationChosenVehicle(vehicle);
            }
        });

        backButton = findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();

                LocalizationFragment localizationFragment = (LocalizationFragment) fm.findFragmentByTag(Constants.LOCALIZATION_FRAGMENT_TAG);
                if (localizationFragment != null && localizationFragment.isVisible()) {
                    restoreMainFragment();
                    return;
                }

                AccidentFragment accidentFragment = (AccidentFragment) fm.findFragmentByTag(Constants.ACCIDENT_FRAGMENT_TAG);
                if (accidentFragment != null && accidentFragment.isVisible()) {
                    showVehicleFragment();
                    return;
                }

                VehicleLocalizationFragment vehicleLocalizationFragment =
                        (VehicleLocalizationFragment) fm.findFragmentByTag(Constants.VEHICLE_LOCALIZATION_FRAGMENT_TAG);

                if (vehicleLocalizationFragment != null && vehicleLocalizationFragment.isVisible()) {
                    showVehicleFragment();
                    return;
                }

                ChooseVehicleFragment chooseVehicleFragment =
                        (ChooseVehicleFragment) fm.findFragmentByTag(Constants.CHOOSE_VEHICLE_FRAGMENT_TAG);

                if (chooseVehicleFragment != null && chooseVehicleFragment.isVisible()) {
                    restoreMainFragment();
                    return;
                }

                VehicleFragment vehicleFragment = (VehicleFragment) fm.findFragmentByTag(Constants.VEHICLES_FRAGMENT_TAG);

                if (vehicleFragment != null && vehicleFragment.isVisible()) {
                    showVehiclesFragment();
                }
            }
        });

        fromDateButton = findViewById(R.id.odDataButton);
        fromDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentDate = mSharedViewModel.getDateFrom();
                pickUpDataTime();
            }
        });

        final Observer<Date> dateFromObserver = new Observer<Date>() {
            @Override
            public void onChanged(@Nullable Date date) {
                String newText = "Od " + simpleDateFormat.format(date);
                fromDateButton.setText(newText);
            }
        };

        mSharedViewModel.getDateFrom().observe(this, dateFromObserver);

        toDateButton = findViewById(R.id.doDataButton);
        toDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentDate = mSharedViewModel.getDateTo();
                pickUpDataTime();
            }
        });

        final Observer<Date> dateToObserver = new Observer<Date>() {
            @Override
            public void onChanged(@Nullable Date date) {
                String newText = "Do " + simpleDateFormat.format(date);
                toDateButton.setText(newText);
            }
        };

        mSharedViewModel.getDateTo().observe(this, dateToObserver);


        clockTextView = findViewById(R.id.widok_data_godzina);

        TextView loggedTextView = findViewById(R.id.widok_zalogowano);
        loggedTextView.setText("Zalogowano jako: " + operatorName);

        currentWinndowView = findViewById(R.id.currentWindowView);

        searchToolbar = findViewById(R.id.searchToolbar);

        searchVehicleTextEdit = findViewById(R.id.wyszukajEditText);
        searchVehicleTextEdit.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchVehicleTextEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filterVehicleList(s.toString());
            }
        });

        searchVehicleButton = findViewById(R.id.searchButton);
        searchVehicleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String currentText = searchVehicleTextEdit.getText().toString();
                filterVehicleList(currentText);
            }
        });

        clearTextEditButton = findViewById(R.id.clearEditTextButton);
        clearTextEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchVehicleTextEdit.setText("");
            }
        });


        homeButton = findViewById(R.id.homeButton);
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restoreMainFragment();
            }
        });

        chosenVehicleTextView = findViewById(R.id.wybranoPojazdTextView);

        Observer<Vehicle> chosenVehicleObserver = new Observer<Vehicle>() {
            @Override
            public void onChanged(@Nullable Vehicle vehicle) {
                String newText = "Wybrano " + vehicle.getName();
                chosenVehicleTextView.setText(newText);
            }
        };

        mSharedViewModel.getChosenVehicle().observe(this, chosenVehicleObserver);

        changeVehicleButton = findViewById(R.id.zmienPojazdButton);
        changeVehicleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showVehiclesFragment();
                hideVehicleFragmentButtons();
            }
        });

        mSharedViewModel.setDefaultDates();
        hideAllUnnecessaryButtons();
    }

    private void filterVehicleList(String text) {
        ArrayList<Vehicle> filteredList = getFilteredList(text);
        refreshVehicleListFragment(filteredList);
    }

    private void refreshVehicleListFragment(ArrayList<Vehicle> filteredList) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentManager childFragmentManager;
        LocalizationFragment localizationFragment = (LocalizationFragment) fm.findFragmentByTag(Constants.LOCALIZATION_FRAGMENT_TAG);
        if (localizationFragment != null && localizationFragment.isVisible()) {
            childFragmentManager = localizationFragment.getChildFragmentManager();
            filterFragmentList(filteredList, childFragmentManager);
            return;
        }

        AccidentFragment accidentFragment = (AccidentFragment) fm.findFragmentByTag(Constants.ACCIDENT_FRAGMENT_TAG);
        if (accidentFragment != null && accidentFragment.isVisible()) {
            childFragmentManager = accidentFragment.getChildFragmentManager();
            filterFragmentList(filteredList, childFragmentManager);
            return;
        }

        VehicleLocalizationFragment vehicleLocalizationFragment =
                (VehicleLocalizationFragment) fm.findFragmentByTag(Constants.VEHICLE_LOCALIZATION_FRAGMENT_TAG);

        if (vehicleLocalizationFragment != null && vehicleLocalizationFragment.isVisible()) {
            childFragmentManager = vehicleLocalizationFragment.getChildFragmentManager();
            filterFragmentList(filteredList, childFragmentManager);
            return;
        }

        ChooseVehicleFragment chooseVehicleFragment =
                (ChooseVehicleFragment) fm.findFragmentByTag(Constants.CHOOSE_VEHICLE_FRAGMENT_TAG);

        if (chooseVehicleFragment != null && chooseVehicleFragment.isVisible()) {
            childFragmentManager = chooseVehicleFragment.getChildFragmentManager();
            filterFragmentList(filteredList, childFragmentManager);
            return;
        }
    }

    private ArrayList<Vehicle> getFilteredList(String text) {
        ArrayList<Vehicle> filteredList = new ArrayList<>();

        if (text.length() != 0) {
            for (Vehicle vehicle : mSharedViewModel.vehicleRepository.getAllVehicles()){
                if (vehicle.Name.toLowerCase().contains(text.toLowerCase())){
                    filteredList.add(vehicle);
                }
            }
        }
        else {
            filteredList = mSharedViewModel.vehicleRepository.getAllVehicles();
        }
        return filteredList;
    }

    private void filterFragmentList(ArrayList<Vehicle> filteredList,
                                    FragmentManager childFragmentManager) {
        VehicleListFragment vehicleListFragment = (VehicleListFragment)
                childFragmentManager.findFragmentByTag(Constants.VEHICLE_LIST_FRAGMENT_TAG);

        if (vehicleListFragment != null) {
            vehicleListFragment.filterList(filteredList);
        }
    }

    private void pickUpDataTime() {
        Calendar c = Calendar.getInstance();
        c.setTime(currentDate.getValue());
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this, MainActivity.this, year, month, day);
        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        // initials to time dialog
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        currentDateTimeHolder.setDate(c.getTime());

        TimePickerDialog timePickerDialog = new TimePickerDialog(MainActivity.this, MainActivity.this, hour, minute, true);
        timePickerDialog.show();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
        c.set(Calendar.MINUTE, minute);
        currentDateTimeHolder.setTime(c.getTime());

        updateDataButton();
    }

    private void updateDataButton() {
        if (currentDate == mSharedViewModel.getDateTo()) {
            mSharedViewModel.setDateTo(currentDateTimeHolder.getFullDate());
        }
        else if (currentDate == mSharedViewModel.getDateFrom()) {
            mSharedViewModel.setDateFrom(currentDateTimeHolder.getFullDate());
        }
    }

    private void replaceMainViewByFragmentWithTag(String tag, boolean addToBackStack) {
        FragmentManager manager = getSupportFragmentManager();
        manager.executePendingTransactions();
        Fragment fragment = manager.findFragmentByTag(tag);
        FragmentTransaction ft = manager.beginTransaction();
        if (fragment == null) {
            ft.replace(R.id.widok_glowny, getNewInstanceFragmentForTag(tag), tag);
        } else {
            ft.replace(R.id.widok_glowny, fragment, tag);
        }

        ft.commitNow();
    }

    public void showVehiclesFragment() {
        replaceMainViewByFragmentWithTag(Constants.CHOOSE_VEHICLE_FRAGMENT_TAG, true);
        showVehiclesButtons();
    }

    private void showVehiclesButtons() {
        Utils.showButton(homeButton);
        Utils.showButton(backButton);

        currentWinndowView.setVisibility(View.VISIBLE);
        currentWinndowView.setEnabled(true);
        currentWinndowView.setImageResource(R.drawable.pojazdy_button);

        searchToolbar.setEnabled(true);
        searchVehicleTextEdit.setText("");
        searchToolbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onAwariaButtonClicked() {
        replaceMainViewByFragmentWithTag(Constants.ACCIDENT_FRAGMENT_TAG, true);

        searchToolbar.setEnabled(true);
        searchVehicleTextEdit.setText("");
        searchToolbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLokalizacjaButtonClicked() {
        Vehicle chosenVehicle = mSharedViewModel.getChosenVehicle().getValue();
        mSharedViewModel.setVehicleLocalizationChosenVehicle(chosenVehicle);

        replaceMainViewByFragmentWithTag(Constants.VEHICLE_LOCALIZATION_FRAGMENT_TAG, true);
        FragmentManager fm = getSupportFragmentManager();
        VehicleLocalizationFragment fragment = (VehicleLocalizationFragment) fm.findFragmentByTag(Constants.VEHICLE_LOCALIZATION_FRAGMENT_TAG);
        if (fragment != null) {
            fragment.showParametersForVehicle(mSharedViewModel.getChosenVehicle().getValue());
        }

        searchToolbar.setEnabled(true);
        searchToolbar.setVisibility(View.VISIBLE);
        searchVehicleTextEdit.setText("");
    }

    @Override
    public void onAcceptButtonClicked() {
        showVehcileFragment();
        //  send accident to server
        boolean isAccidentRegistered = reportDamageOfVehicle();
        if (isAccidentRegistered) {
            String message= "Awaria została zarejestrowana";
            Utils.showToastWithMessage(getApplicationContext(), message);
        }
    }

    private boolean reportDamageOfVehicle() {
        return mSharedViewModel.reportDamageOfVehicle();
    }

    private void showVehcileFragment() {
        replaceMainViewByFragmentWithTag(Constants.VEHICLES_FRAGMENT_TAG, false);
        searchToolbar.setVisibility(View.INVISIBLE);
        searchToolbar.setEnabled(false);
    }


    public void restoreMainFragment() {
        replaceMainViewByFragmentWithTag(Constants.MAIN_FRAGMENT_TAG, false);
        hideAllUnnecessaryButtons();
    }

    public void showLocalizationFragment() {
        replaceMainViewByFragmentWithTag(Constants.LOCALIZATION_FRAGMENT_TAG, true);
        mSharedViewModel.setDefaultDates();
        showLocalisationButtons();
    }

    private void showLocalisationButtons() {
        Utils.showButton(loadHistoryOfVehicleButton);
        Utils.showButton(homeButton);
        Utils.showButton(backButton);
        Utils.showButton(fromDateButton);
        Utils.showButton(toDateButton);

        Utils.showImageView(currentWinndowView);
        currentWinndowView.setImageResource(R.drawable.historia_lokalizacji_button);

        searchToolbar.setEnabled(true);
        searchVehicleTextEdit.setText("");
        searchToolbar.setVisibility(View.VISIBLE);
    }

    private void hideAllUnnecessaryButtons() {
        Utils.hideButton(loadHistoryOfVehicleButton);
        Utils.hideButton(homeButton);
        Utils.hideButton(backButton);
        Utils.hideButton(fromDateButton);
        Utils.hideButton(toDateButton);
        Utils.hideButton(changeVehicleButton);
        Utils.hideImageView(currentWinndowView);
        Utils.hideTextView(chosenVehicleTextView);
        searchToolbar.setEnabled(false);
        searchToolbar.setVisibility(View.INVISIBLE);


    }

    @Override
    public void onChooseVehicleButtonClicked() {
        showVehicleFragment();
    }

    private void showVehicleFragment() {
        replaceMainViewByFragmentWithTag(Constants.VEHICLES_FRAGMENT_TAG, false);
        Utils.showButton(changeVehicleButton);
        Utils.showTextView(chosenVehicleTextView);

        searchToolbar.setEnabled(false);
        searchToolbar.setVisibility(View.INVISIBLE);
    }

    private void hideVehicleFragmentButtons() {
        Utils.hideButton(changeVehicleButton);
        Utils.hideTextView(chosenVehicleTextView);
        searchToolbar.setEnabled(false);
        searchToolbar.setVisibility(View.INVISIBLE);
    }

    private Fragment getNewInstanceFragmentForTag(String tag) {
        switch (tag){
            case Constants.LOCALIZATION_FRAGMENT_TAG: {
                return LocalizationFragment.newInstance();
            }
            case Constants.VEHICLES_FRAGMENT_TAG: {
                return VehicleFragment.newInstance();
            }
            case Constants.CHOOSE_VEHICLE_FRAGMENT_TAG: {
                return ChooseVehicleFragment.newInstance();
            }
            case Constants.MAIN_FRAGMENT_TAG: {
                return MainFragment.newInstance();
            }
            case Constants.VEHICLE_LOCALIZATION_FRAGMENT_TAG: {
                return VehicleLocalizationFragment.newInstance();
            }
            case Constants.ACCIDENT_FRAGMENT_TAG: {
                return AccidentFragment.newInstance();
            }
            default:
                return null;
        }
    }



    private class ClockRunner implements Runnable{
        @Override
        public void run() {
            while(!Thread.currentThread().isInterrupted()){
                try {
                    doWork();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }catch(Exception e){
                }
            }
        }
    }

    public void doWork() {
        runOnUiThread(new Runnable() {
            public void run() {
                try{
                    String curTime = clockDateFromat.format(new Date());
                    clockTextView.setText(curTime);
                }catch (Exception e) {}
            }
        });
    }

    private class DateTimeHolder {
        private Date date;
        private Date time;

        public void setDate(Date date) {
            this.date = date;
        }

        public void setTime(Date time) {
            this.time = time;
        }

        public Date getFullDate() {
            Calendar c1 = Calendar.getInstance();
            c1.setTime(date);
            Calendar c2 = Calendar.getInstance();
            c2.setTime(time);

            c1.set(Calendar.HOUR_OF_DAY, c2.get(Calendar.HOUR_OF_DAY));
            c1.set(Calendar.MINUTE, c2.get(Calendar.MINUTE));

            return c1.getTime();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
}
