package com.example.ipr.ipr_project.Serialization;

public class ConfigurationSheet {

    public enum VehicleType {
        Land,
        Air,
        Water
    }

    public VehicleType VehicleType;
    public InputConfiguration InputConfiguration;
}
