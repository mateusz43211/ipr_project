package com.example.ipr.ipr_project.Serialization;

import java.util.Date;

public class Damage {
    public Date DateStart;
    public Date DateEnd;
    public String VehicleId;
    public String OperatorId;
    public String TechnicianId;
}
