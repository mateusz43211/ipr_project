package com.example.ipr.ipr_project.Serialization;

import java.util.List;

public class Vehicle {
    public String Id;
    public String Name;
    public List<Damage> Damages;
    public ConfigurationSheet ConfigurationSheet;
    public VehicleStatus LastStatus;

    public String getName() {
        return Name;
    }

    @Override
    public String toString() {
        return Name;
    }
}
