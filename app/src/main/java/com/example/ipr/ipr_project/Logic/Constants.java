package com.example.ipr.ipr_project.Logic;

public class Constants {
    public static final int BAD_LOGIN_PASSWORD_CODE = -1;
    public static final int CONNECTION_NOT_WORKING = -2;
}
