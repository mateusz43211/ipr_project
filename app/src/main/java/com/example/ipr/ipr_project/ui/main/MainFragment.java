package com.example.ipr.ipr_project.ui.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.ipr.ipr_project.MainActivity;
import com.example.ipr.ipr_project.R;

public class MainFragment extends Fragment {

    private ImageButton vehiclesButton;
    private ImageButton localizationHistoryButton;

    public static MainFragment newInstance()
    {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.main_fragment, container, false);
        vehiclesButton = rootView.findViewById(R.id.pojazdy_button);
        vehiclesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity activity = (MainActivity)getActivity();
                activity.showVehiclesFragment();
            }
        });

        localizationHistoryButton = rootView.findViewById(R.id.historia_lokalizacji_button);
        localizationHistoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity activity = (MainActivity)getActivity();
                activity.showLocalizationFragment();
            }
        });
        return rootView;
    }
}
