package com.example.ipr.ipr_project.ui.main;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.ipr.ipr_project.Constants;
import com.example.ipr.ipr_project.R;
import com.example.ipr.ipr_project.Serialization.Vehicle;
import com.example.ipr.ipr_project.SharedViewModel;

public class ChooseVehicleFragment extends Fragment {

    private OnChooseVehicleFragmentInteractionListener mListener;
    TextView chosenVehicleextView;
    ConstraintLayout buttonsLayout;
    ImageButton zatwierzWyborButton;
    private SharedViewModel mSharedViewModel;

    public static ChooseVehicleFragment newInstance() {
        return new ChooseVehicleFragment();
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_choose_vehicle, container, false);
        chosenVehicleextView = rootView.findViewById(R.id.wybranoPojazdTextView);
        buttonsLayout = rootView.findViewById(R.id.buttonsLayout);
        buttonsLayout.setVisibility(View.INVISIBLE);
        buttonsLayout.setEnabled(false);

        zatwierzWyborButton = rootView.findViewById(R.id.zatwierdzWyborButton);
        zatwierzWyborButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onChooseVehicleButtonClicked();
            }
        });

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnChooseVehicleFragmentInteractionListener) {
            mListener = (OnChooseVehicleFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                                               + " must implement OnChooseVehicleFragmentInteractionListener");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Fragment childFragment = new VehicleListFragment();
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.replace(R.id.child_fragment_container, childFragment, Constants.VEHICLE_LIST_FRAGMENT_TAG);
        ft.commitNow();

        buttonsLayout.setEnabled(false);
        buttonsLayout.setVisibility(View.INVISIBLE);

    }

    public interface OnChooseVehicleFragmentInteractionListener {
        void onChooseVehicleButtonClicked();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSharedViewModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);

        final Observer<Vehicle> vehicleObserver = new Observer<Vehicle>() {
            @Override
            public void onChanged(@Nullable Vehicle vehicle) {
                String newText = "Wybrano " + vehicle.getName();
                chosenVehicleextView.setText(newText);
                if (!buttonsLayout.isShown()) {
                    buttonsLayout.setEnabled(true);
                    buttonsLayout.setVisibility(View.VISIBLE);
                }
            }
        };

        mSharedViewModel.getChosenVehicle().observe(this, vehicleObserver);
    }
}
