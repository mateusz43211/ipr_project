package com.example.ipr.ipr_project.ui.main;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.ipr.ipr_project.Logic.ParameterList;
import com.example.ipr.ipr_project.R;
import com.example.ipr.ipr_project.Serialization.Vehicle;
import com.example.ipr.ipr_project.SharedViewModel;

public class ParameterListFragment extends ListFragment {
    ParameterList parameters = new ParameterList();
    ArrayAdapter<String> adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedViewModel model = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);

        final Observer<Vehicle> vehicleObserver = new Observer<Vehicle>() {
            @Override
            public void onChanged(@Nullable Vehicle vehicle) {
                parameters.setParameters(vehicle);
                adapter.notifyDataSetChanged();
            }
        };

        model.getVehicleLocalizationChosenVehicle().observe(this, vehicleObserver);
        adapter = new ArrayAdapter<>(getActivity(), R.layout.list_item, parameters);
        setListAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}