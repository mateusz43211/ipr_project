package com.example.ipr.ipr_project.ui.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ipr.ipr_project.Constants;
import com.example.ipr.ipr_project.R;

public class LocalizationFragment extends Fragment {

    public LocalizationFragment() {
        // Required empty public constructor
    }

    public static LocalizationFragment newInstance() {
        return new LocalizationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_localization, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Fragment childFragment = new VehicleListFragment();
        Fragment mapFragment = HistoryMapFragment.newInstance();
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.replace(R.id.child_fragment_container, childFragment, Constants.VEHICLE_LIST_FRAGMENT_TAG);
        ft.replace(R.id.map_container, mapFragment);
        ft.hide(mapFragment);
        ft.commitNow();
    }
}
