package com.example.ipr.ipr_project.ui.main;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.ipr.ipr_project.R;
import com.example.ipr.ipr_project.SharedViewModel;
import com.example.ipr.ipr_project.Utils;

public class VehicleFragment extends Fragment {

    private OnVehicleFragmentInteractionListener mListener;
    private ImageButton awariaButton, sterujButton, lokalizacjaButton;

    public interface OnVehicleFragmentInteractionListener {
        void onAwariaButtonClicked();
        void onLokalizacjaButtonClicked();
    }

    public static VehicleFragment newInstance() {
        return new VehicleFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.vehicle_fragment, container, false);

        awariaButton = rootView.findViewById(R.id.awariaButton);
        awariaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onAwariaButtonClicked();
            }
        });

        sterujButton = rootView.findViewById(R.id.sterujButton);
        sterujButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = "Tutaj pojawia się panel sterowania, który jest zewnętrzną aplikacją";
                Utils.showToastWithMessage(getActivity(), message);
            }
        });

        lokalizacjaButton = rootView.findViewById(R.id.lokalizacjaButton);
        lokalizacjaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onLokalizacjaButtonClicked();
            }
        });
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnVehicleFragmentInteractionListener) {
            mListener = (OnVehicleFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                                               + " must implement OnChooseVehicleFragmentInteractionListener");
        }
    }
}
